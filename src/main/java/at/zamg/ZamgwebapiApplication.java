package at.zamg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZamgwebapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZamgwebapiApplication.class, args);
	}
}
